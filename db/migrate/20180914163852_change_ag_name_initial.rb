class ChangeAgNameInitial < ActiveRecord::Migration
  def change
    rename_column :operations, :ag_name_initial, :bank_number_initial
  end
end
