class RemoveOpTypeFromOperation < ActiveRecord::Migration
  def change
    remove_column :operations, :op_type, :string
  end
end
