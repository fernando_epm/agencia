class CreateAgencies < ActiveRecord::Migration
  def change
    create_table :agencies do |t|
      t.string :ag_name
      t.string :ag_number
      t.string :ag_address
      t.string :string

      t.timestamps null: false
    end
  end
end
