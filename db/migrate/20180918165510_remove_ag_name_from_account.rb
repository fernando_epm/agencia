class RemoveAgNameFromAccount < ActiveRecord::Migration
  def change
    remove_column :accounts, :ag_name, :string
    remove_column :accounts, :ag_number, :string
  end
end
