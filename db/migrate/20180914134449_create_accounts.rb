class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :ag_name
      t.string :ag_number
      t.string :acc_number
      t.float :acc_limit

      t.timestamps null: false
    end
  end
end
