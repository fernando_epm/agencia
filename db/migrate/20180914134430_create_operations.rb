class CreateOperations < ActiveRecord::Migration
  def change
    create_table :operations do |t|
      t.string :ag_name_initial
      t.string :ag_number_initial
      t.string :acc_number_initial
      t.string :ag_name_final
      t.string :ag_number_final
      t.string :acc_number_final
      t.string :op_type
      t.float :op_value
      t.date :op_date

      t.timestamps null: false
    end
  end
end
