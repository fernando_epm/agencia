class AddInitialAccountIdToOperation < ActiveRecord::Migration
  def change
    add_column :operations, :initial_account_id, :integer
    add_column :operations, :final_account_id, :integer
  end
end
