class RemoveBankNumberFromOperation < ActiveRecord::Migration
  def change
    remove_column :operations, :bank_number_initial, :string
    remove_column :operations, :ag_number_initial, :string
    remove_column :operations, :acc_number_initial, :string
    remove_column :operations, :bank_number_final, :string
    remove_column :operations, :ag_number_final, :string
    remove_column :operations, :acc_number_final, :string
  end
end
