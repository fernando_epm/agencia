class AddAgencyIdToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :agency_id, :integer
    add_column :accounts, :user_id, :integer
  end
end
