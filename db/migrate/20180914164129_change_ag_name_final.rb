class ChangeAgNameFinal < ActiveRecord::Migration
  def change
    rename_column :operations, :ag_name_final, :bank_number_final
  end
end
