class AddBankIdToAgency < ActiveRecord::Migration
  def change
    add_column :agencies, :bank_id, :integer
  end
end
