class AddOpTypeToOperation < ActiveRecord::Migration
  def change
    add_column :operations, :op_type, :integer
  end
end
