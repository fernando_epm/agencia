# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Bank seeds

bank_list = [
  [ "Bradesco", "237" ],
  [ "Caixa", "104" ],
  [ "Santander", "033" ],
  [ "Itaú", "341" ],
  [ "Banco do Brasil", "001" ]
]

bank_list.each do |name, number|
  Bank.create( bank_name: name, bank_number: number )
end

# Agency seeds

# agency_list = [
#   [ "Bradesco", "237" ],
#   [ "Caixa", "104" ],
#   [ "Santander", "033" ],
#   [ "Itaú", "341" ],
#   [ "Banco do Brasil", "001" ]
# ]

# agency_list.each do |name, number, address|
#   Agency.create( ag_name: name, ag_number: number, ag_address: address )
# end

# Account seeds

# account_list = [
#   [ "Bradesco", "237" ],
#   [ "Caixa", "104" ],
#   [ "Santander", "033" ],
#   [ "Itaú", "341" ],
#   [ "Banco do Brasil", "001" ]
# ]

# account_list.each do |name, number, address|
#   Account.create( acc_name: name, acc_number: number, acc_address: address )
# end
