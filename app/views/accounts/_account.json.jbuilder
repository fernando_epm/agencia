json.extract! account, :id, :acc_number, :acc_limit, :created_at, :updated_at
json.url account_url(account, format: :json)
