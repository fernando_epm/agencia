json.extract! operation, :id, :initial_account_id, :final_account_id, :op_type, :op_value, :op_date, :created_at, :updated_at
json.url operation_url(operation, format: :json)
