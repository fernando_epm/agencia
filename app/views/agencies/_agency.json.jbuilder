json.extract! agency, :id, :bank_id, :ag_name, :ag_number, :ag_address, :created_at, :updated_at
json.url agency_url(agency, format: :json)
