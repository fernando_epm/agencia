class Agency < ActiveRecord::Base
  belongs_to :bank
  has_many :accounts

  validates_presence_of :bank_id, :ag_number

  def number_with_bank
    "#{ag_number} (#{bank.bank_name})"
  end
end
