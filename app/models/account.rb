class Account < ActiveRecord::Base
  belongs_to :user
  belongs_to :agency
  has_many :operations

  validates_presence_of :agency_id, :acc_number, :acc_limit
end
