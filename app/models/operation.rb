class Operation < ActiveRecord::Base
  # Mandatory
  belongs_to :initial_account, :class_name => "Account", :foreign_key => "initial_account_id"

  # Not required if the operation is deposit or withdrawl
  belongs_to :final_account, :class_name => "Account", :foreign_key => "final_account_id"

  enum op_type: {
    deposit: 1,
    withdrawl: 2,
    transfer: 3
  }

  validates_presence_of :initial_account_id, :op_type, :op_value
  validate :check_limit
  validates_presence_of :final_account_id, if: :transfer?
  after_create :update_limit

  def self.enum_capitalizer
    Operation.op_types.to_a.map{|op_type| [op_type.first.capitalize, op_type.first]}
  end

  def check_limit
    if !deposit? && initial_account.present? && (initial_account.acc_limit < op_value)
      errors.add(:op_value, "Insuficient funds in the account.")
    end
  end

  def update_limit
    if deposit?
      initial_account.update(acc_limit: initial_account.acc_limit + op_value)
    end
    if withdrawl?
      initial_account.update(acc_limit: initial_account.acc_limit - op_value)
    end
    if transfer?
      initial_account.update(acc_limit: initial_account.acc_limit - op_value)
      final_account.update(acc_limit: final_account.acc_limit + op_value)
    end
  end

end
