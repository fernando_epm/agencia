class Bank < ActiveRecord::Base
  has_many :agencies

  validates_presence_of :bank_name, :bank_number

  def name_with_number
    "#{self.bank_name} - #{self.bank_number}"
  end
end
