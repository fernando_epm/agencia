require 'test_helper'

class OperationsControllerTest < ActionController::TestCase
  setup do
    @operation = operations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:operations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create operation" do
    assert_difference('Operation.count') do
      post :create, operation: { acc_number_final: @operation.acc_number_final, acc_number_initial: @operation.acc_number_initial, ag_name_final: @operation.ag_name_final, ag_name_initial: @operation.ag_name_initial, ag_number_final: @operation.ag_number_final, ag_number_initial: @operation.ag_number_initial, op_date: @operation.op_date, op_type: @operation.op_type, op_value: @operation.op_value }
    end

    assert_redirected_to operation_path(assigns(:operation))
  end

  test "should show operation" do
    get :show, id: @operation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @operation
    assert_response :success
  end

  test "should update operation" do
    patch :update, id: @operation, operation: { acc_number_final: @operation.acc_number_final, acc_number_initial: @operation.acc_number_initial, ag_name_final: @operation.ag_name_final, ag_name_initial: @operation.ag_name_initial, ag_number_final: @operation.ag_number_final, ag_number_initial: @operation.ag_number_initial, op_date: @operation.op_date, op_type: @operation.op_type, op_value: @operation.op_value }
    assert_redirected_to operation_path(assigns(:operation))
  end

  test "should destroy operation" do
    assert_difference('Operation.count', -1) do
      delete :destroy, id: @operation
    end

    assert_redirected_to operations_path
  end
end
